# # Accepters
provider "aws" {
  region = var.region
  alias  = "peer"
  assume_role {
    role_arn = "arn:aws:iam::${var.account_id}:role/z5x_jenkins"
  }

}
# Requester
provider "aws" {
  region = var.region
  assume_role {
    role_arn = "arn:aws:iam::${var.account_id}:role/z5x_jenkins"
  }
}
# Requester's side of the connection.
resource "aws_vpc_peering_connection" "peer" {
  vpc_id        = data.aws_vpc.main.id
  peer_vpc_id   = data.aws_vpc.vpcid.id
  peer_owner_id = data.aws_caller_identity.peer.account_id #AWS Account ID
  peer_region   = "ap-south-1"
  auto_accept   = false
  tags = {
    Side = "Requester"
    Name = "hipi-${var.environment}-peering"
  }
}
# Accepter's side of the connection.
resource "aws_vpc_peering_connection_accepter" "peer" {
  provider                  = aws.peer
  vpc_peering_connection_id = aws_vpc_peering_connection.peer.id
  auto_accept               = true

  tags = {
    Side = "Accepter"
    Name = "${var.environment}-infra-peering"
  }
}

##############################################################
##############PRIVATE_REDIS_ROUTE_TABLE#####################

output "route_id_redis1" {
  value = data.aws_route_table.redis-prd-rtb-pri-az1.id
}
resource "aws_route" "route_redis1" {
  provider                  = aws.peer
  route_table_id            = data.aws_route_table.redis-prd-rtb-pri-az1.id
  destination_cidr_block    = var.vpc_cidr_destination
  vpc_peering_connection_id = aws_vpc_peering_connection.peer.id
}
output "route_id_redis2" {
  value = data.aws_route_table.redis-prd-rtb-pri-az2.id
}
resource "aws_route" "route_redis2" {
  provider                  = aws.peer
  route_table_id            = data.aws_route_table.redis-prd-rtb-pri-az2.id
  destination_cidr_block    = var.vpc_cidr_destination
  vpc_peering_connection_id = aws_vpc_peering_connection.peer.id
}
output "route_id_redis3" {
  value = data.aws_route_table.redis-prd-rtb-pri-az3.id
}
resource "aws_route" "route_redis3" {
  provider                  = aws.peer
  route_table_id            = data.aws_route_table.redis-prd-rtb-pri-az3.id
  destination_cidr_block    = var.vpc_cidr_destination
  vpc_peering_connection_id = aws_vpc_peering_connection.peer.id
}
##############PRIVATE_RDS_ROUTE_TABLE#####################

output "route_id_rds1" {
  value = data.aws_route_table.rds-prd-rtb-pri-az1.id
}
resource "aws_route" "route_rds1" {
  provider                  = aws.peer
  route_table_id            = data.aws_route_table.rds-prd-rtb-pri-az1.id
  destination_cidr_block    = var.vpc_cidr_destination
  vpc_peering_connection_id = aws_vpc_peering_connection.peer.id
}
output "route_id_rds2" {
  value = data.aws_route_table.rds-prd-rtb-pri-az2.id
}
resource "aws_route" "route_rds2" {
  provider                  = aws.peer
  route_table_id            = data.aws_route_table.rds-prd-rtb-pri-az2.id
  destination_cidr_block    = var.vpc_cidr_destination
  vpc_peering_connection_id = aws_vpc_peering_connection.peer.id
}
output "route_id_rds3" {
  value = data.aws_route_table.rds-prd-rtb-pri-az3.id
}
resource "aws_route" "route_rds3" {
  provider                  = aws.peer
  route_table_id            = data.aws_route_table.rds-prd-rtb-pri-az3.id
  destination_cidr_block    = var.vpc_cidr_destination
  vpc_peering_connection_id = aws_vpc_peering_connection.peer.id
}
##############PRIVATE_COMPUTE_ROUTE_TABLE#####################
output "route_id_compute1" {
  value = data.aws_route_table.compute-prd-rtb-pri-az1.id
}
resource "aws_route" "route_compute1" {
  provider                  = aws.peer
  route_table_id            = data.aws_route_table.compute-prd-rtb-pri-az1.id
  destination_cidr_block    = var.vpc_cidr_destination
  vpc_peering_connection_id = aws_vpc_peering_connection.peer.id
}
output "route_id_compute2" {
  value = data.aws_route_table.compute-prd-rtb-pri-az2.id
}
resource "aws_route" "route_compute2" {
  provider                  = aws.peer
  route_table_id            = data.aws_route_table.compute-prd-rtb-pri-az2.id
  destination_cidr_block    = var.vpc_cidr_destination
  vpc_peering_connection_id = aws_vpc_peering_connection.peer.id
}
output "route_id_compute3" {
  value = data.aws_route_table.rds-prd-rtb-pri-az3.id
}
resource "aws_route" "route_compute3" {
  provider                  = aws.peer
  route_table_id            = data.aws_route_table.compute-prd-rtb-pri-az3.id
  destination_cidr_block    = var.vpc_cidr_destination
  vpc_peering_connection_id = aws_vpc_peering_connection.peer.id
}
############################DO_NOT_CHANGE##########################
