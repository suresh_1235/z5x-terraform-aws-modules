variable "device_name" {
  description = "The device name to expose to the instance (for example, /dev/sdh or xvdh)"
}
variable "volume_id" {
  description = "ID of the Volume to be attached"
}
variable "instance_id" {
  description = "ID of the Instance to attach to"
}