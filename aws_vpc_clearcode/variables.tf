variable "search_pattern" {
  default = "ssrv-demo*"
}
variable "vpc_id" {
  description = "VPC ID"
}