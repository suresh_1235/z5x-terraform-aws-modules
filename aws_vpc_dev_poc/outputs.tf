output "vpc_id" {
  value = data.aws_vpc.vpc.id
}
output "vpc_id_cidr" {
  value = data.aws_vpc.vpc.cidr_block
}
output "subnet_pri_az1_id" {
  value = data.aws_subnet.pri_subnet_1.id
}
output "subnet_pri_az2_id" {
  value = data.aws_subnet.pri_subnet_2.id
}
output "subnet_pri_az3_id" {
  value = data.aws_subnet.pri_subnet_3.id
}
output "subnet_pri_az4_id" {
  value = data.aws_subnet.pri_subnet_4.id
}
output "subnet_pri_az7_id" {
  value = data.aws_subnet.pri_subnet_7.id
}
output "subnet_pri_az8_id" {
  value = data.aws_subnet.pri_subnet_8.id
}

output "subnet_pub_az1_id" {
  value = data.aws_subnet.pub_subnet_1.id
}
output "subnet_pub_az2_id" {
  value = data.aws_subnet.pub_subnet_2.id
}

output "subnet_db_az1_id" {
  value = data.aws_subnet.db_subnet_1.id
}
output "subnet_db_az2_id" {
  value = data.aws_subnet.db_subnet_2.id
}

output "subnet_pri_ids" {
  value = data.aws_subnet_ids.pri.ids
}
output "subnet_pub_ids" {
  value = data.aws_subnet_ids.pub.ids
}
output "subnet_db_ids" {
  value = data.aws_subnet_ids.db.ids
}