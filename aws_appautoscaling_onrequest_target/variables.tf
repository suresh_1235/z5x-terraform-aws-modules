variable "name" {
  description = "Name of the ECS Policy created, will appear in Auto Scaling under Service in ECS"
  type        = string
}

variable "alb_arn_suffix" {
  description = "ARN Suffix (not full ARN) of the Application Load Balancer for use with CloudWatch. Output attribute from LB resource: `arn_suffix`"
  type        = string
  default = null
}

#variable "target_group_arn_suffix" {
#  description = "ALB Target Group ARN Suffix (not full ARN) for use with CloudWatch. Output attribute from Target Group resource: `arn_suffix`"
#  type        = string
#}

variable "ecs_cluster_name" {
   description = "Name of cluster"
}

variable "ecs_service_name" {
   description = "Name of the service"
}
 
variable "scale_in_cooldown" {
  description = "Time between scale in action"
  default     = 300
  type        = number
}

variable "scale_out_cooldown" {
  description = "Time between scale out action"
  default     = 300
  type        = number
}


variable "target_value" {
  description = "Requests per target in target group metrics to trigger scaling activity"
  type        = number
}

variable "min_count" {
  description = "Time between scale in action"
  type        = number
}

variable "max_count" {
  description = "Time between scale out action"
  type        = number
}

variable "disable_scale_in" {
  description = "Disable scale-in action, defaults to false"
  default     = false
  type        = bool
}

variable "predefined_metric_type" {
   description = "Metric type"
}

