####################################################################
#### Created by : Kalyan Chakravarthi/ Kalyan Bhave ####
#### Date : 20-Jun-2020 ####
#### Updated by : Balakrishna/ Praveen kumar/ Sathish kumar ####
####################################################################
####################    VPC    ####################
data "aws_vpc" "vpc" {
  tags = {
    Name = var.vpc_id
  }
}
######################## singpore region ###############################################
data "aws_subnet" "sharedsp_subnet_1" {
  vpc_id = data.aws_vpc.vpc.id
  filter {
    name   = "tag:Name"
    values = ["${var.search_pattern}-pri-sharedsp-az1"]
  }
}
data "aws_subnet" "sharedsp_subnet_2" {
  vpc_id = data.aws_vpc.vpc.id
  filter {
    name   = "tag:Name"
    values = ["${var.search_pattern}-pri-sharedsp-az2"]
  }
}