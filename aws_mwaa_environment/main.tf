####################################################################
#### Created by : Balakrishna ####
#### Date : 19-JUL-2021 ####
####################################################################
resource "aws_mwaa_environment" "example" {
  dag_s3_path                    = var.dag_s3_path
  execution_role_arn             = var.execution_role_arn
  environment_class              = var.environment_class
  max_workers                    = var.max_workers
  min_workers                    = var.min_workers
  name                           = var.name
  webserver_access_mode          = var.webserver_access_mode
  requirements_s3_path           = var.requirements_s3_path
  requirements_s3_object_version = var.requirements_s3_object_version
  airflow_configuration_options = {
    "smtp.smtp_host"      = var.smtp_host
    "smtp.smtp_mail_from" = var.smtp_mail_from
    "smtp.smtp_password"  = var.smtp_password
    "smtp.smtp_port"      = var.smtp_port
    "smtp.smtp_ssl"       = var.smtp_ssl
    "smtp.smtp_starttls"  = var.smtp_starttls
    "smtp.smtp_user"      = var.smtp_user
  }
  logging_configuration {
    dag_processing_logs {
      enabled   = true
      log_level = var.dag_processing_logs
    }

    scheduler_logs {
      enabled   = true
      log_level = var.scheduler_logs
    }

    task_logs {
      enabled   = true
      log_level = var.task_logs
    }

    webserver_logs {
      enabled   = true
      log_level = var.webserver_logs
    }

    worker_logs {
      enabled   = true
      log_level = var.worker_logs
    }
  }
  network_configuration {
    security_group_ids = var.security_group_ids
    subnet_ids         = var.subnet_ids
  }
  source_bucket_arn = var.source_bucket_arn
  tags = {
    Name        = var.name
    Environment = var.environment
  }
}