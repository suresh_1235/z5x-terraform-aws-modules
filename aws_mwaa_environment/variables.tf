variable "execution_role_arn" {
  description = "The Amazon Resource Name (ARN) of the task execution role that the Amazon MWAA and its environment can assume"
}
variable "name" {
  description = "name of the service"
}
variable "security_group_ids" {
  description = "Security groups IDs for the environment. At least one of the security group needs to allow MWAA resources to talk to each other, otherwise MWAA cannot be provisioned."
}
variable "subnet_ids" {
  description = "The private subnet IDs in which the environment should be created. MWAA requires two subnets."
}
variable "source_bucket_arn" {
  description = "The Amazon Resource Name (ARN) of your Amazon S3 storage bucket. For example, arn:aws:s3:::airflow-mybucketname."
}
variable "environment" {
  description = "Name of the environment"
}
variable "environment_class" {
  description = "Environment class for the cluster. Possible options are mw1.small, mw1.medium, mw1.large. Will be set by default to mw1.small"
}
variable "max_workers" {
  description = "The maximum number of workers that can be automatically scaled up. Value need to be between 1 and 25. Will be 10 by default."
}
variable "min_workers" {
  description = "The minimum number of workers that you want to run in your environment. Will be 1 by default."
}
variable "dag_s3_path" {
  description = "The relative path to the DAG folder on your Amazon S3 storage bucket."
}
variable "webserver_access_mode" {
  description = "(Optional) Specifies whether the webserver should be accessible over the internet or via your specified VPC. Possible options: PRIVATE_ONLY (default) and PUBLIC_ONLY"
}
variable "dag_processing_logs" {
  description = "Log configuration options for processing DAGs; Valid values: CRITICAL, ERROR, WARNING, INFO, DEBUG. Will be INFO by default"
}
variable "scheduler_logs" {
  description = "Log configuration options for the schedulers; Valid values: CRITICAL, ERROR, WARNING, INFO, DEBUG. Will be INFO by default"
}
variable "task_logs" {
  description = "Log configuration options for DAG tasks; Valid values: CRITICAL, ERROR, WARNING, INFO, DEBUG. Will be INFO by default"
}
variable "webserver_logs" {
  description = "Log configuration options for the webservers; Valid values: CRITICAL, ERROR, WARNING, INFO, DEBUG. Will be INFO by default"
}
variable "worker_logs" {
  description = "Log configuration options for the worker; Valid values: CRITICAL, ERROR, WARNING, INFO, DEBUG. Will be INFO by default"
}

variable "smtp_host" {
  default = null
}
variable "smtp_mail_from" {
  default = null
}
variable "smtp_password" {
  default = null
}
variable "smtp_port" {
  default = null
}
variable "smtp_ssl" {
  default = null
}
variable "smtp_starttls" {
  default = null
}
variable "smtp_user" {
  default = null
}
variable "requirements_s3_path" {
  default = null
  description = "requirements file path"
}
variable "requirements_s3_object_version" {
  default = null
}