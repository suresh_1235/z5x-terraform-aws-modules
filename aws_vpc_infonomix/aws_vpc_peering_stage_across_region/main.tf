## Requsters ap-southeast-1
provider "aws" {
  region = var.req_region
  #  alias = "peering"
  assume_role {
    role_arn = "arn:aws:iam::${var.account_id}:role/z5x_jenkins"
  }
}
# # Accepters ap-south-1
provider "aws" {
  region = var.region
  alias = "peer"
  assume_role {
    role_arn = "arn:aws:iam::${var.account_id}:role/z5x_jenkins"
  }
}
# Requester's side of the connection.
resource "aws_vpc_peering_connection" "peer" {
  vpc_id        = data.aws_vpc.main.id
  peer_vpc_id   = data.aws_vpc.vpcid.id
  peer_owner_id = data.aws_caller_identity.peer.account_id #AWS Account ID
  peer_region   = "ap-south-1"
  auto_accept   = false
  tags = {
    Side = "Requester"
    Name = "Mwaa-${var.environment}-peering"
  }
}
# Accepter's side of the connection.
resource "aws_vpc_peering_connection_accepter" "peer" {
  provider                  = aws.peer
  vpc_peering_connection_id = aws_vpc_peering_connection.peer.id
  auto_accept               = true

  tags = {
    Side = "Accepter"
    Name = "${var.environment}-mwaa-peering"
  }
}