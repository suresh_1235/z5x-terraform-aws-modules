variable "region" {
  description = "AWS Regions"
}
variable "account_id" {
  description = "AWS Account ID"
}
#variable "vpc_cidr_source" {
#  description = "VPC Source CIDR"
#}
#variable "vpc_cidr_destination" {
#  description = "Destination VPC CIDR "
#}
variable "environment" {
  description = "Name of the Environment"
}
variable "req_region" {}
