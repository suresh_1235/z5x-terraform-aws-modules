output "vpc_id" {
  value = data.aws_vpc.vpc.id
}
output "vpc_id_cidr" {
  value = data.aws_vpc.vpc.cidr_block
}
output "subnet_pub_az1_id" {
  value = data.aws_subnet.pub_subnet_1.id
}
output "subnet_pub_az2_id" {
  value = data.aws_subnet.pub_subnet_2.id
}
output "subnet_pub_az3_id" {
  value = data.aws_subnet.pub_subnet_3.id
}
output "subnet_rds_az1_id" {
  value = data.aws_subnet.rds_subnet_1.id
}
output "subnet_rds_az2_id" {
  value = data.aws_subnet.rds_subnet_2.id
}
output "subnet_rds_az3_id" {
  value = data.aws_subnet.rds_subnet_3.id
}
output "subnet_redis_az1_id" {
  value = data.aws_subnet.redis_subnet_1.id
}
output "subnet_redis_az2_id" {
  value = data.aws_subnet.redis_subnet_2.id
}
output "subnet_redis_az3_id" {
  value = data.aws_subnet.redis_subnet_3.id
}
output "subnet_compute_az1_id" {
  value = data.aws_subnet.compute_subnet_1.id
}
output "subnet_compute_az2_id" {
  value = data.aws_subnet.compute_subnet_2.id
}
output "subnet_compute_az3_id" {
  value = data.aws_subnet.compute_subnet_3.id
}